# README #

This project is an implementation of task for a company Synertrade
### Compile ###
To compile this source JDK 1.8 should be used. 
It's required to have maven installed and probably the internet access to download
plugins and a dependency. 

### Run ###

In a /target folder after successful compilation You will find tech.jar executable file.

`java -jar tech.jar -h` command will output possible options.

Executing the jar without options will perform the task logic to mentioned page 
[https://en.wikipedia.org/wiki/Europe](https://en.wikipedia.org/wiki/Europe)