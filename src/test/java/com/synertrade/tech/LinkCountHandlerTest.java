package com.synertrade.tech;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

/**
 * Simple test for our logic.
 *
 * @author ekiriche
 * @since 1.0
 */


public class LinkCountHandlerTest {

    private static final String HTTP_MAIN_LINK = "http://main.link";
    private static final String HTTP_FIRST_LINK = "http://first.link";
    private static final String HTTP_SECOND_LINK = "http://second.link";
    private static final String HTTP_THIRD_LINK = "http://third.link";

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private static final String MAIN_LINK = "<html>" +
            "<a href='http://first.link'>First</a>" +
            "<a href='http://second.link'>Second</a>" +
            "<a href='http://third.link'>Third</a>" +
            "</html>";

    // first page contains 1 link to second
    private static final String FIRST_LINK = "<html>" +
            "<a href='http://second.link'>Second</a>" +
            "</html>";

    // second page contains 1 link to first
    private static final String SECOND_LINK = "<html>" +
            "<a href='http://first.link'>First</a>" +
            "</html>";

    // third page contains 1 link to first
    private static final String THIRD_LINK = "<html>" +
            "<a href='http://first.link'>First</a>" +
            "</html>";

    // so we have 3 link to first, 2 links to second and 1 link to third

    private InputStream stringToIn(final String source) {
        return new ByteArrayInputStream(source.getBytes(StandardCharsets.UTF_8));
    }

    @Test
    public void testLinks() throws IOException {

        final LinkCountHandler linkCountHandler = Mockito.spy(new LinkCountHandler(null, 0));

        Mockito.doReturn(stringToIn(MAIN_LINK)).when(linkCountHandler).downloadFile(HTTP_MAIN_LINK);
        Mockito.doReturn(stringToIn(FIRST_LINK)).when(linkCountHandler).downloadFile(HTTP_FIRST_LINK);
        Mockito.doReturn(stringToIn(SECOND_LINK)).when(linkCountHandler).downloadFile(HTTP_SECOND_LINK);
        Mockito.doReturn(stringToIn(THIRD_LINK)).when(linkCountHandler).downloadFile(HTTP_THIRD_LINK);

        linkCountHandler.parseUrl(HTTP_MAIN_LINK, "any");

        assertEquals(linkCountHandler.getData().getCountByLinkUrl(HTTP_FIRST_LINK), new Long(3));
        assertEquals(linkCountHandler.getData().getCountByLinkUrl(HTTP_SECOND_LINK), new Long(2));
        assertEquals(linkCountHandler.getData().getCountByLinkUrl(HTTP_THIRD_LINK), new Long(1));
    }

}
