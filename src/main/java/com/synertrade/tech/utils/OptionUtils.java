package com.synertrade.tech.utils;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

/**
 * Utility class for CLI apache options.
 *
 * @author ekiriche
 * @since 1.0
 */
public class OptionUtils {

    // according to the task
    public static final String DEFAULT_URL = "https://en.wikipedia.org/wiki/Europe";

    // define possible options for CLI
    public static final Options OPTIONS;

    // helper option
    public static final HelpFormatter HELP_FORMATTER = new HelpFormatter();

    // initialize options
    static {
        OPTIONS = new Options();

        // option for specifying output file name
        OPTIONS.addOption(
                Option.builder("out")
                        .hasArg()
                        .argName("filename")
                        .desc("file to keep output")
                        .build()
        );

        // option for specifying html page
        OPTIONS.addOption(
                Option.builder("page")
                        .hasArg()
                        .argName("url")
                        .desc(String.format("URL of page to parse, be default is %s", DEFAULT_URL))
                        .build()
        );

        // proxy
        OPTIONS.addOption(
                Option.builder("proxy")
                        .hasArg()
                        .argName("proxy host and port")
                        .desc("Proxy settings if needed")
                        .build()
        );

        // option to list a help
        OPTIONS.addOption(new Option("h", "print this message"));
    }

}


