package com.synertrade.tech.utils;

import com.synertrade.tech.structure.Link;
import com.synertrade.tech.structure.LinkConcurrentMap;
import me.tongfei.progressbar.ProgressBar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.ZonedDateTime;
import java.util.LinkedHashMap;
import java.util.Queue;

/**
 * Utility class for io formatting.
 *
 * @author Evgeniy Kirichenko
 * @version 2.0
 * @since 2.0
 */
public class IOUtils {

    private IOUtils(){}

    private static final String fileHead =
            "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "    <head>\n" +
            "        <title>Report for %s</title>\n" +
            "        <meta charset=\"UTF-8\">\n" +
            "        <style>\n" +
            "            h3 {\n" +
            "                text-align: center;\n" +
            "            }\n" +
            "            .report-table {\n" +
            "                border: solid 1px #DDEEEE;\n" +
            "                border-collapse: collapse;\n" +
            "                border-spacing: 0;\n" +
            "                font: normal 13px Arial, sans-serif;\n" +
            "            }\n" +
            "            .report-table thead th {\n" +
            "                background-color: #DDEFEF;\n" +
            "                border: solid 1px #DDEEEE;\n" +
            "                color: #336B6B;\n" +
            "                padding: 10px;\n" +
            "                text-align: left;\n" +
            "                text-shadow: 1px 1px 1px #fff;\n" +
            "            }\n" +
            "            .report-table tbody td {\n" +
            "                border: solid 1px #DDEEEE;\n" +
            "                color: #333;\n" +
            "                padding: 10px;\n" +
            "                text-shadow: 1px 1px 1px #fff;\n" +
            "                word-wrap: break-word;\n" +
            "            }\n" +
            "            .table-container {\n" +
            "                padding: 50px;\n" +
            "                max-width: 650px;\n" +
            "            }\n" +
            "        </style>\n" +
            "    </head>\n" +
            "    <body>\n" +
            "        <h3>Report for %s</h3>\n" +
            "        <div class=\"table-container\">\n" +
            "            <table class=\"report-table\">\n" +
            "                <thead>\n" +
            "                <tr>\n" +
            "                    <th>Link</th>\n" +
            "                    <th>Frequency</th>\n" +
            "                </tr>\n" +
            "                </thead>\n" +
            "                <tbody>\n";


    private static final String fileBottom =

            "                </tbody>\n" +
            "            </table>\n" +
            "        </div>\n" +
            "    </body>\n" +
            "</html>\n";


    /**
     * Write output map.
     *
     * @param url      the url
     * @param filename the filename
     * @param data     the data
     * @throws IOException the io exception
     */
    public static void writeOutputMap(final String url, final String filename, final LinkConcurrentMap data)
            throws IOException {

        final LinkedHashMap<Link, Long> collect = data.expose();

        // output file creating
        final File file = new File(filename);
        if (file.exists() && file.isFile()) {
            // if exists - clear it
            file.delete();
        }
        file.createNewFile();

        try (PrintWriter writer = new PrintWriter(file);
             ProgressBar pb = new ProgressBar("Writing results", collect.size())) {
            writer.printf(fileHead, url, url);
                collect.forEach((link, aLong) -> {
                    writer.printf("<tr>\n" + "<td>%s</td>\n" + "<td>%d</td>\n" + "</tr>\n", link, aLong);
                    pb.step();
                });
            writer.write(fileBottom);
        }
    }

    /**
     * Write error data.
     *
     * @param errors the errors
     * @throws FileNotFoundException the file not found exception
     */
    public static void writeErrorData(final Queue<String> errors) throws FileNotFoundException {
        final File errorFile = new File("errors.out");
        try (final PrintWriter errorWriter = new PrintWriter(errorFile)) {
            errorWriter.printf("Error log. %tc %n", ZonedDateTime.now());
            if (!errors.isEmpty()) {
                errors.forEach(errorWriter::println);
            } else {
                errorWriter.println("No errors occurred");
            }
        }
    }

    /**
     * Print hello title.
     */
    public static void printHelloTitle() {
        System.out.println("###################################################");
        System.out.println("### Test task from E. Kirichenko for Synertrade ###");
        System.out.println("###              For help type 'h'              ###");
        System.out.println("###################################################");
        System.out.println();
    }
}
