package com.synertrade.tech;

import com.synertrade.tech.structure.Link;
import com.synertrade.tech.structure.LinkConcurrentMap;
import com.synertrade.tech.utils.IOUtils;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.LongAdder;


/**
 * Class that implements main logic.
 *
 * @author ekiriche
 * @since 1.0
 */
class LinkCountHandler {

    private boolean useProxy;

    private static final int MAX_URL_LENGTH_TO_REPRESENT = 60;
    private final ExecutorService es;
    private final Proxy proxy;
    private final LinkConcurrentMap data = new LinkConcurrentMap();
    private final CountDownLatch latch = new CountDownLatch(1);
    private final Queue<String> errors = new ConcurrentLinkedQueue<>();
    private final LongAdder queueSize = new LongAdder();
    private final ProgressBar progressBar;

    /**
     * The type Child page parser.
     */
    class ChildPageParser implements Runnable {

        private String url;
        /**
         * The Is main thread.
         */
        boolean isMainThread;

        /**
         * Instantiates a new Child page parser.
         *
         * @param url          the url
         * @param isMainThread the is main thread
         */
        ChildPageParser(final String url, final boolean isMainThread) {
            this.url = url;
            this.isMainThread = isMainThread;
        }

        private void step() {
            if (!isMainThread) {
                progressBar.step();
            }
        }

        @Override
        public void run() {
            final Elements elements;
            try {
                elements = Jsoup.parse(downloadFile(url), "UTF-8", url).select("a");
            } catch (final IOException e) {
                errors.add(e.getMessage());
                step();
                return;
            }

            Element a;
            for (int i = 0; i < elements.size(); i++) {
                a = elements.get(i);
                String foundUrl = a.attr("href");
                if (foundUrl.startsWith("#")) continue;
                String description = a.text().trim();
                final String targetUrl;
                try {
                    targetUrl = foundUrl.startsWith("http") ? foundUrl : new URL(new URL(url), foundUrl).toString();
                } catch (final MalformedURLException e) {
                    errors.add(String.format("Url %s has incorrect format: %s", url, e.getMessage()));
                    step();
                    return;
                }
                if (description.isEmpty() || description.startsWith("<")) {
                    description = targetUrl.length() > MAX_URL_LENGTH_TO_REPRESENT
                            ? targetUrl.substring(0, MAX_URL_LENGTH_TO_REPRESENT) + "..." : targetUrl;
                }
                final Link childLink = new Link(targetUrl, description);
                data.computeIfAbsent(childLink, k -> new LongAdder()).increment();
                if (isMainThread) {
                    es.submit(new ChildPageParser(targetUrl, false));
                    if (i == elements.size() - 1) {
                        if (progressBar.getMax() == Integer.MAX_VALUE) {
                            progressBar.maxHint(queueSize.sum() - 1);
                        }
                        latch.countDown();
                    } else {
                        queueSize.increment();
                    }
                }
            }
            step();
        }
    }

    /**
     * Gets data.
     *
     * @return the data
     */
    LinkConcurrentMap getData() {
        return data;
    }

    /**
     * Instantiates a new Url handler.
     *
     * @param host the host
     * @param port the port
     */
    LinkCountHandler(final String host, final int port) {
        this.useProxy = host != null && !host.isEmpty();
        proxy = useProxy ? new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port)) : null;

        progressBar = new ProgressBar("Performing the task", Integer.MAX_VALUE,
                500, System.err, ProgressBarStyle.COLORFUL_UNICODE_BLOCK, " thread", 1);
        es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 4);
    }

    /**
     * Download a file.
     *
     * @param url the file url
     * @return the input stream
     * @throws IOException the io exception
     */
    InputStream downloadFile(final String url) throws IOException {
            return useProxy ? new BufferedInputStream(new URL(url).openConnection(proxy).getInputStream()) :
                    new BufferedInputStream(new URL(url).openConnection().getInputStream());
    }

    /**
     * Parse url.
     *
     * @param UrlString the url
     * @param filename  the filename
     * @throws IOException the io exception
     */
    void parseUrl(final String UrlString, final String filename) throws IOException {
        final long startProcessingTime = System.currentTimeMillis();
        es.submit(new ChildPageParser(UrlString, true));
        try {
            latch.await();
            es.shutdown();
            es.awaitTermination(3, TimeUnit.MINUTES);
        } catch (final InterruptedException e) {
            System.out.println("Task was interrupted: " + e.getMessage());
        }
        progressBar.close();
        final long totalProcessingTime = System.currentTimeMillis() - startProcessingTime;
        System.out.printf("%n Task has finished after: %d milliseconds %n", totalProcessingTime);
        System.out.printf("%n Found %d links %n", data.entrySet().size());

        // write a file with data
        IOUtils.writeOutputMap(UrlString, filename, data);

        // write errors
        IOUtils.writeErrorData(errors);
    }
}
