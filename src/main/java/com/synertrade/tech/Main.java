package com.synertrade.tech;

import com.synertrade.tech.utils.IOUtils;
import org.apache.commons.cli.*;

import java.io.IOException;

import static com.synertrade.tech.utils.OptionUtils.*;

/**
 * Main class used for user CLI interaction.
 *
 * @author Evgeniy Kirichenko
 * @version 2.0
 * @since 2.0
 */
public class Main {

    public static void main(String [] args) {
        IOUtils.printHelloTitle();

        final CommandLineParser parser = new DefaultParser();
        try {
            // parse the command line arguments
            final CommandLine line = parser.parse(OPTIONS, args);

            // output the help message
            if (line.hasOption("h")) {
                HELP_FORMATTER.printHelp( "tech", OPTIONS);
            }
            String url = DEFAULT_URL;
            // if user has defined another page - parse it instead of default
            if(line.hasOption("page")) {
                url = line.getOptionValue("page");
                System.out.printf("Scanning page: %s %n", url);

            }

            final String filename = line.hasOption("out") ? line.getOptionValue("out") : "output.html";

            String proxyHost = null;
            int proxyPort = -1;
            if (line.hasOption("proxy")) {
                final String[] splittedProxy = line.getOptionValue("proxy").split(":");
                proxyHost = splittedProxy[0];
                proxyPort = Integer.parseInt(splittedProxy[1]);
            }

            new LinkCountHandler(proxyHost, proxyPort).parseUrl(url, filename);
        }
        catch(final ParseException exp ) {
            System.out.println(exp.getMessage());
            HELP_FORMATTER.printHelp( "tech", OPTIONS);
        }
        catch (final IOException e) {
            System.out.println("Could not parse the page.");
            System.out.println(e.getMessage());
        }
    }
}

