package com.synertrade.tech.structure;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Collectors;

/**
 * Wrap chm with our helper methods.
 *
 * @author ekiriche
 * @since 1.0
 */
public class LinkConcurrentMap extends ConcurrentHashMap<Link, LongAdder> {

    /**
     * Expose linked hash map.
     *
     * @return the linked hash map
     */
    public LinkedHashMap<Link, Long> expose() {
        return entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, o -> o.getValue().sum()))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey,
                                Map.Entry::getValue,
                                (oldValue, newValue) -> oldValue,
                                LinkedHashMap::new
                        )
                );
    }

    /**
     * Gets count by link url.
     *
     * @param url the url
     * @return the count by link url
     */
    public Long getCountByLinkUrl(final String url) {
        return entrySet()
                .stream()
                .filter(link -> link.getKey().getUrl().equalsIgnoreCase(url))
                .findFirst()
                .map(linkLongAdderEntry -> linkLongAdderEntry.getValue().sum())
                .orElse(-1L);
    }

}
