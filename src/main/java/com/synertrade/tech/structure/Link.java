package com.synertrade.tech.structure;

import java.util.Objects;

/**
 * DTO class to keep link and description all together.
 *
 * @author ekiriche
 * @since 1.0
 */
public class Link {
    private String url;
    private String description;

    /**
     * Instantiates a new Link.
     *
     * @param url         the url
     * @param description the description
     */
    public Link(String url, String description) {
        this.url = url;
        this.description = description;
    }

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets url.
     *
     * @param url the url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Link link = (Link) o;
        return Objects.equals(url, link.url);
    }

    @Override
    public int hashCode() {
        return url.hashCode();
    }

    @Override
    public String toString() {
        return "<a href='" +  this.url + "'>" + this.description + "</a>";
    }
}